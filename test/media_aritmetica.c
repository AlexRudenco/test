#include <stdio.h>
#include <stdlib.h>

#define MAX 1000

// function to sort the array in ascending order
void Array_sort(int *array , int n)
{
    // declare some local variables
    int i=0 , j=0 , temp=0;

    for(i=0 ; i<n ; i++)
    {
        for(j=0 ; j<n-1 ; j++)
        {
            if(array[j]>array[j+1])
            {
                temp        = array[j];
                array[j]    = array[j+1];
                array[j+1]  = temp;
            }
        }
    }

    printf("\nThe array after sorting is..\n");
    for(i=0 ; i<n ; i++)
    {
        printf("\narray_1[%d] : %d",i,array[i]);
    }
}

// function to calculate the median of the array
float Find_median(int array[] , int n)
{
    float median=0;

    // if number of elements are even
    if(n%2 == 0)
        median = (array[(n-1)/2] + array[n/2])/2.0;
    // if number of elements are odd
    else
        median = array[n/2];

    return median;
}


int main(){

    FILE *myFile;
    char *name="numbers.txt";

    myFile = fopen("numbers.txt", "r");

    if (myFile == NULL){              /* diagnostics to stderr, add relevant detail, such as the filename */
        fprintf(stderr, "Error Reading File:%s\n", name);
        exit (0);
     }

    //read file into array
    int numberArray[MAX];
    int count = 0;
    int i;
    float median=0;

    for (i = 0; (i < 1000) && (!feof(myFile)); i++){
        fscanf(myFile, "%d,", &numberArray[i] );
        count++;
    }

    // Sort the array in ascending order
    Array_sort(numberArray , count);

    // the median of your array.
    median = Find_median(numberArray , count);

    printf("\n\nThe median is : %.2f\n",median);

    //printf("number is: %d\n\n",count);


    fclose(myFile);

    return 0;
}
